/* This is the base JS for Snowmeister's Canvas Experiments */
// #todo: FOUC avoidance - Preloader, HIDE un-initialised elements etc..
String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};
// Lets be Modular!...
var Sce = (function($) {
  _.templateSettings.variable = "sce";
  // Shortcut, and we can EXPORT this...
  var _me = this;
  var defaultStrokeColor = '#000000';
  var defaultFillColor = '#ffcc00';
  var renderdata = {
    x: 20,
    y: 20,
    cstop1: 'rgba(41, 10, 89, 1.000)',
    cstop2: 'rgba(255, 124, 0, 1.000)'
  };
  var body = $('body');
  var expanded = false;
  // Draw a SQUARE using CANVAS with no LIBRARY
  _me.canvasdemoDrawingsquares = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    ctx.rect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    ctx.stroke();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // CREATE A horizontal LINEAR Gradient CANVAS with no LIBRARY
  _me.canvasdemoHorizontalgradient = function(id) {
    var canvas = document.getElementById(id),
      ctx = canvas.getContext('2d'),
      grd;
    // Create gradient
    grd = ctx.createLinearGradient(renderdata.defaults.rh, 0.000, renderdata.defaults.rh, renderdata.defaults.rh);
    // Add colors
    grd.addColorStop(0.000, renderdata.cstop1);
    grd.addColorStop(1.000, renderdata.cstop2);
    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, renderdata.defaults.rw, renderdata.defaults.rh);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // CREATE A VERTICAL LINEAR Gradient CANVAS with no LIBRARY
  _me.canvasdemoVerticalgradient = function(id) {
    var canvas = document.getElementById(id),
      ctx = canvas.getContext('2d'),
      grd;
    // Create gradient
    grd = ctx.createLinearGradient(0.000, renderdata.defaults.rw / 2, renderdata.defaults.rw, renderdata.defaults.rw / 2);
    // Add colors
    grd.addColorStop(0.000, renderdata.cstop1);
    grd.addColorStop(1.000, renderdata.cstop2);
    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(renderdata.x, renderdata.y, renderdata.defaults.rw, renderdata.defaults.rh);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // CREATE A RADIAL LINEAR Gradient CANVAS with no LIBRARY
  _me.canvasdemoRadialgradient = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext('2d');
    var x = 100,
      y = 75,
      // Radii of the white glow.
      innerRadius = 5,
      outerRadius = 70,
      // Radius of the entire circle.
      radius = 60;
    var grd = ctx.createRadialGradient(x, y, innerRadius, x, y, outerRadius);
    grd.addColorStop(0.000, renderdata.cstop2);
    grd.addColorStop(1.000, renderdata.cstop1);
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.fillStyle = grd;
    ctx.fill();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // Draw a CIRCLE using CANVAS with no LIBRARY
  _me.canvasdemoDrawingcircle = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    var centerX = (renderdata.defaults.square / 2) + renderdata.x;
    var centerY = (renderdata.defaults.square / 2) + renderdata.y;
    var radius = (renderdata.defaults.square / 2);
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    ctx.lineWidth = 1;
    ctx.strokeStyle = defaultStrokeColor;
    ctx.stroke();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // Draw an ELLIPSE using CANVAS with no LIBRARY
  _me.canvasdemoDrawingellipse = function(id) {
    var canvas = document.getElementById(id);
    var context = canvas.getContext('2d');
    var centerX = (renderdata.defaults.square / 2.5) + renderdata.x;
    var centerY = (renderdata.defaults.square / 2.5) + renderdata.y;
    var radius = (renderdata.defaults.square / 2.5);
    // save state
    context.save();
    // scale context horizontally
    context.scale(2, 1);
    // draw circle which will be stretched into an oval
    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    // restore to original state
    context.restore();
    // apply styling
    context.lineWidth = 1;
    context.strokeStyle = defaultStrokeColor;
    context.stroke();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a POLYGON using CANVAS with no LIBRARY
  //points: number of points (or number of sides for polygons)
  //radius1: "outer" radius of the star
  //radius2: "inner" radius of the star (if equal to radius1, a polygon is drawn)
  //angle0: initial angle (clockwise), by default, stars and polygons are 'pointing' up
  _me.canvasdemoDrawingpolygon = function(id) {
    var canvas = document.getElementById(id);
    var context = canvas.getContext('2d');
    this.drawShape = function(ctx, x, y, points, radius1, radius2, alpha0) {
      var i, angle, radius;
      if (radius2 !== radius1) {
        points = 2 * points;
      }
      for (i = 0; i <= points; i++) {
        angle = i * 2 * Math.PI / points - Math.PI / 2 + alpha0;
        radius = i % 2 === 0 ? radius1 : radius2;
        context.lineTo(x + radius * Math.cos(angle), y + radius * Math.sin(angle));
      }
    };
    context.beginPath();
    this.drawShape(context, 100, 100, 3, 80, 80, 0);
    context.strokeStyle = defaultStrokeColor;
    context.fillStyle = defaultFillColor;
    context.lineWidth = 1;
    context.stroke();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a POLYGON using CANVAS with no LIBRARY
  _me.canvasdemoFillingpolygon = function(id) {
    var canvas = document.getElementById(id);
    var context = canvas.getContext('2d');
    this.drawShape = function(ctx, x, y, points, radius1, radius2, alpha0) {
      var i, angle, radius;
      if (radius2 !== radius1) {
        points = 2 * points;
      }
      for (i = 0; i <= points; i++) {
        angle = i * 2 * Math.PI / points - Math.PI / 2 + alpha0;
        radius = i % 2 === 0 ? radius1 : radius2;
        context.lineTo(x + radius * Math.cos(angle), y + radius * Math.sin(angle));
      }
    };
    context.beginPath();
    this.drawShape(context, 100, 100, 3, 80, 80, 0);
    context.fillStyle = defaultFillColor;
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = null;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL an ELLIPSE using CANVAS with no LIBRARY
  _me.canvasdemoFillingellipse = function(id) {
    var canvas = document.getElementById(id);
    var context = canvas.getContext('2d');
    var centerX = (renderdata.defaults.square / 2.5) + renderdata.x;
    var centerY = (renderdata.defaults.square / 2.5) + renderdata.y;
    var radius = (renderdata.defaults.square / 2.5);
    // save state
    context.save();
    // scale context horizontally
    context.scale(2, 1);
    // draw circle which will be stretched into an oval
    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    // restore to original state
    context.restore();
    // apply styling
    context.fillStyle = defaultFillColor;
    context.fill();
    context.lineWidth = 0;
    context.strokeStyle = null;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // Draw a RECTANGLE using CANVAS with no LIBRARY
  _me.canvasdemoDrawingrectangle = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    ctx.rect(renderdata.x, renderdata.y, renderdata.defaults.rw, renderdata.defaults.rh);
    ctx.stroke();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a RECTANGLE using CANVAS with no LIBRARY
  _me.canvasdemoFillingrectangle = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = defaultFillColor;
    ctx.fillRect(renderdata.x, renderdata.y, renderdata.defaults.rw, renderdata.defaults.rh);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a SQUARE using CANVAS with no LIBRARY
  _me.canvasdemoFillingsquares = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = defaultFillColor;
    ctx.fillRect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a CIRCLE using CANVAS with no LIBRARY
  _me.canvasdemoFillingcircle = function(id) {
    var canvas = document.getElementById(id);
    var ctx = canvas.getContext("2d");
    var centerX = (renderdata.defaults.square / 2) + renderdata.x;
    var centerY = (renderdata.defaults.square / 2) + renderdata.y;
    var radius = (renderdata.defaults.square / 2);
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = defaultFillColor;
    ctx.fill();
    $('#' + id + '-loader').remove();
  };
  // CREATE A horizontal LINEAR Gradient CANVAS with no LIBRARY
  _me.jcanvasdemoHorizontalgradient = function(id) {
    // Create and store a linear gradient
    var gradient = $('#' + id).createGradient({
      // Gradient is drawn relative to layer position
      x1: 0,
      y1: renderdata.defaults.rh,
      x2: 0,
      y2: renderdata.defaults.rh / 2,
      c1: renderdata.cstop2,
      c2: renderdata.cstop1
    });
    // Create layer with gradient fill
    $('#' + id).drawRect({
      fillStyle: gradient,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // CREATE A vertical LINEAR Gradient CANVAS with no LIBRARY
  _me.jcanvasdemoVerticalgradient = function(id) {
    // Create and store a linear gradient
    var gradient = $('#' + id).createGradient({
      // Gradient is drawn relative to layer position
      y1: 0,
      x1: renderdata.defaults.rw,
      y2: 0,
      x2: renderdata.defaults.rw / 2,
      c1: renderdata.cstop2,
      c2: renderdata.cstop1
    });
    // Create layer with gradient fill
    $('#' + id).drawRect({
      fillStyle: gradient,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };



  // DRAW a SQUARE using jCanvas
  _me.jcanvasdemoDrawingsquares = function(id) {
    $('#' + id).drawRect({
      strokeStyle: defaultStrokeColor,
      strokeWidth: 1,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.square,
      height: renderdata.defaults.square,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a CIRCLE using jCanvas
  _me.jcanvasdemoDrawingcircle = function(id) {
    $('#' + id).drawEllipse({
      strokeStyle: defaultStrokeColor,
      strokeWidth: 1,
      x: (renderdata.defaults.square / 2) + renderdata.x,
      y: (renderdata.defaults.square / 2) + renderdata.y,
      width: renderdata.defaults.square,
      height: renderdata.defaults.square
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // draw a polygon using jCanvas
  _me.jcanvasdemoDrawingpolygon = function(id) {
    $('#' + id).drawPolygon({
      strokeStyle: defaultStrokeColor,
      strokeWidth: 1,
      x: renderdata.x * 4,
      y: renderdata.y * 4,
      radius: renderdata.defaults.rh / 1.5,
      sides: 3
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // fill a polygon using jCanvas
  _me.jcanvasdemoFillingpolygon = function(id) {
    $('#' + id).drawPolygon({
      fillStyle: defaultFillColor,
      x: renderdata.x * 4,
      y: renderdata.y * 4,
      radius: renderdata.defaults.rh / 1.5,
      sides: 3
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a RECTANGLE using jCanvas
  _me.jcanvasdemoDrawingrectangle = function(id) {
    $('#' + id).drawRect({
      strokeStyle: defaultStrokeColor,
      strokeWidth: 1,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a SQUARE using jCanvas
  _me.jcanvasdemoFillingsquares = function(id) {
    $('#' + id).drawRect({
      fillStyle: defaultFillColor,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.square,
      height: renderdata.defaults.square,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a RECTANGLE using jCanvas
  _me.jcanvasdemoFillingrectangle = function(id) {
    $('#' + id).drawRect({
      fillStyle: defaultFillColor,
      x: renderdata.x,
      y: renderdata.y,
      layer: true,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      fromCenter: false,
      scale: 1
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a CIRCLE using jCanvas
  _me.jcanvasdemoFillingcircle = function(id) {
    $('#' + id).drawEllipse({
      fillStyle: defaultFillColor,
      x: (renderdata.defaults.square / 2) + renderdata.x,
      y: (renderdata.defaults.square / 2) + renderdata.y,
      width: renderdata.defaults.square,
      height: renderdata.defaults.square
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW an ELLIPSE using jCanvas
  _me.jcanvasdemoDrawingellipse = function(id) {
    $('#' + id).drawEllipse({
      strokeStyle: defaultStrokeColor,
      strokeWidth: 1,
      x: (renderdata.defaults.square / 2) + (renderdata.x * 2.5),
      y: (renderdata.defaults.square / 2) + renderdata.y,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL an ELLIPSE using jCanvas
  _me.jcanvasdemoFillingellipse = function(id) {
    $('#' + id).drawEllipse({
      fillStyle: defaultFillColor,
      x: (renderdata.defaults.square / 2) + (renderdata.x * 2.5),
      y: (renderdata.defaults.square / 2) + renderdata.y,
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
    });
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a SQUARE using FabricJS
  _me.fabricjsdemoDrawingsquares = function(id) {
    var canvas = new fabric.Canvas(id);
    var rect = new fabric.Rect();
    rect.set({
      width: renderdata.defaults.square,
      height: renderdata.defaults.square,
      top: renderdata.y,
      left: renderdata.x,
      fill: null,
      strokeWidth: 1,
      stroke: defaultStrokeColor
    });
    canvas.add(rect);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a CIRCLE using FabricJS
  _me.fabricjsdemoDrawingcircle = function(id) {
    var canvas = new fabric.Canvas(id);
    var circle = new fabric.Circle({
      radius: renderdata.defaults.square / 2,
      strokeWidth: 1,
      stroke: defaultStrokeColor,
      fill: null,
      left: renderdata.x,
      top: renderdata.y
    });
    canvas.add(circle);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a polygon using FabricJS
  _me.fabricjsdemoDrawingpolygon = function(id) {
    var canvas = new fabric.Canvas(id);
    var triangle = new fabric.Triangle({
      width: renderdata.defaults.rh * 1.25,
      height: renderdata.defaults.rh * 1.25,
      strokeWidth: 1,
      stroke: defaultStrokeColor,
      fill: null,
      left: renderdata.x,
      top: renderdata.y
    });
    canvas.add(triangle);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a polygon using FabricJS
  _me.fabricjsdemoFillingpolygon = function(id) {
    var canvas = new fabric.Canvas(id);
    var triangle = new fabric.Triangle({
      width: renderdata.defaults.rh * 1.25,
      height: renderdata.defaults.rh * 1.25,
      stroke: null,
      fill: defaultFillColor,
      left: renderdata.x,
      top: renderdata.y
    });
    canvas.add(triangle);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW an ELLIPSE using FabricJS
  _me.fabricjsdemoDrawingellipse = function(id) {
    var canvas = new fabric.Canvas(id);
    var ellipse = new fabric.Ellipse({
      left: renderdata.x,
      top: renderdata.y,
      rx: renderdata.defaults.rw / 2,
      ry: renderdata.defaults.rh / 2,
      fill: null,
      stroke: defaultStrokeColor,
      strokeWidth: 1
    });
    canvas.add(ellipse);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL an ELLIPSE using FabricJS
  _me.fabricjsdemoFillingellipse = function(id) {
    var canvas = new fabric.Canvas(id);
    var ellipse = new fabric.Ellipse({
      left: renderdata.x,
      top: renderdata.y,
      rx: renderdata.defaults.rw / 2,
      ry: renderdata.defaults.rh / 2,
      fill: defaultFillColor,
      stroke: null,
    });
    canvas.add(ellipse);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a CIRCLE using FabricJS
  _me.fabricjsdemoFillingcircle = function(id) {
    var canvas = new fabric.Canvas(id);
    var circle = new fabric.Circle({
      radius: renderdata.defaults.square / 2,
      left: renderdata.x,
      fill: defaultFillColor,
      stroke: null,
      top: renderdata.y
    });
    canvas.add(circle);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a SQUARE using FabricJS
  _me.fabricjsdemoDrawingrectangle = function(id) {
    var canvas = new fabric.Canvas(id);
    var rect = new fabric.Rect();
    rect.set({
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      top: renderdata.y,
      left: renderdata.x,
      fill: null,
      strokeWidth: 1,
      stroke: defaultStrokeColor
    });
    canvas.add(rect);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a SQUARE using FabricJS
  _me.fabricjsdemoFillingsquares = function(id) {
    var canvas = new fabric.Canvas(id);
    var rect = new fabric.Rect();
    rect.set({
      width: renderdata.defaults.square,
      height: renderdata.defaults.square,
      top: renderdata.y,
      left: renderdata.x,
      fill: defaultFillColor,
      stroke: null
    });
    canvas.add(rect);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a RECTANGLE using FabricJS
  _me.fabricjsdemoFillingrectangle = function(id) {
    var canvas = new fabric.Canvas(id);
    var rect = new fabric.Rect();
    rect.set({
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      top: renderdata.y,
      left: renderdata.x,
      fill: defaultFillColor,
      stroke: null
    });
    canvas.add(rect);
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a SQUARE using PaperJS
  _me.paperjsdemoDrawingsquares = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    var path = new paper.Path.Rectangle(square);
    path.strokeColor = defaultStrokeColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW an ELLIPSE using PaperJS
  _me.paperjsdemoDrawingellipse = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    var path = new paper.Path.Ellipse({
      point: [renderdata.x, renderdata.y],
      size: [renderdata.defaults.rw, renderdata.defaults.rh],
      fillColor: null
    });
    path.strokeColor = defaultStrokeColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL an ELLIPSE using PaperJS
  _me.paperjsdemoFillingellipse = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    var path = new paper.Path.Ellipse({
      point: [renderdata.x, renderdata.y],
      size: [renderdata.defaults.rw, renderdata.defaults.rh],
      fillColor: defaultFillColor
    });
    // path.strokeColor = defaultStrokeColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a Circle using PaperJS
  _me.paperjsdemoDrawingcircle = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var myCircle = new paper.Path.Circle(new paper.Point((renderdata.defaults.square / 2) + renderdata.x, (renderdata.defaults.square / 2) + renderdata.y), renderdata.defaults.square / 2);
    //var circle = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    //var path = new paper.Path.Rectangle(square);
    myCircle.strokeColor = defaultStrokeColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a RECTANGLE using PaperJS
  _me.paperjsdemoDrawingrectangle = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.rw, renderdata.defaults.rh));
    var path = new paper.Path.Rectangle(square);
    path.strokeColor = defaultStrokeColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a POLYGON using PaperJS
  _me.paperjsdemoDrawingpolygon = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var triangle = new paper.Path.RegularPolygon(new paper.Point(renderdata.x * 4, renderdata.y * 5), 3, renderdata.defaults.rh / 1.25);
    triangle.strokeColor = defaultStrokeColor;
    //triangle.selected = true;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a POLYGON using PaperJS
  _me.paperjsdemoFillingpolygon = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var triangle = new paper.Path.RegularPolygon(new paper.Point(renderdata.x * 4, renderdata.y * 5), 3, renderdata.defaults.rh / 1.25);
    triangle.fillColor = defaultFillColor;
    //triangle.selected = true;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a SQUARE using PaperJS
  _me.paperjsdemoFillingsquares = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    var path = new paper.Path.Rectangle(square);
    //path.strokeColor = '#000000';
    path.fillColor = defaultFillColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a CIRCLE using PaperJS
  _me.paperjsdemoFillingcircle = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    // setup our square
    var myCircle = new paper.Path.Circle(new paper.Point((renderdata.defaults.square / 2) + renderdata.x, (renderdata.defaults.square / 2) + renderdata.y), renderdata.defaults.square / 2);
    //var circle = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.square, renderdata.defaults.square));
    //var path = new paper.Path.Rectangle(square);
    myCircle.fillColor = defaultFillColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a RECTANGLE using PaperJS
  _me.paperjsdemoFillingrectangle = function(id) {
    // Get a reference to the canvas object
    var canvas = document.getElementById(id);
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);
    var square = new paper.Rectangle(new paper.Point(renderdata.x, renderdata.y), new paper.Point(renderdata.defaults.rw, renderdata.defaults.rh));
    var path = new paper.Path.Rectangle(square);
    //path.strokeColor = '#000000';
    path.fillColor = defaultFillColor;
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a SQUARE using EaselJs
  _me.easeljsdemoDrawingsquares = function(id) {
    var stage = new createjs.Stage(id);
    var rect = new createjs.Shape();
    rect.graphics.setStrokeStyle(1).beginStroke(defaultStrokeColor).drawRect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    stage.addChild(rect);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a POLYGON using EaselJs
  _me.easeljsdemoDrawingpolygon = function(id) {
    var stage = new createjs.Stage(id);
    var poly = new createjs.Shape();
    poly.graphics.setStrokeStyle(1).beginStroke(defaultStrokeColor).drawPolyStar(renderdata.x * 5, renderdata.y * 5, renderdata.defaults.square / 2, 3, 0, -90); //.drawRect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    stage.addChild(poly);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a POLYGON using EaselJs
  _me.easeljsdemoFillingpolygon = function(id) {
    var stage = new createjs.Stage(id);
    var poly = new createjs.Shape();
    poly.graphics.beginFill(defaultFillColor).drawPolyStar(renderdata.x * 5, renderdata.y * 5, renderdata.defaults.square / 2, 3, 0, -90); //.drawRect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    stage.addChild(poly);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a CIRCLE using EaselJs
  _me.easeljsdemoDrawingcircle = function(id) {
    //Create a stage by getting a reference to the canvas
    var stage = new createjs.Stage(id);
    //Create a Shape DisplayObject.
    var circle = new createjs.Shape();
    circle.graphics.setStrokeStyle(1).beginStroke(defaultStrokeColor).drawCircle(renderdata.x * 2, renderdata.x * 2, (renderdata.defaults.square / 2));
    //Set position of Shape instance.
    circle.x = circle.y = 50;
    //Add Shape instance to stage display list.
    stage.addChild(circle);
    //Update stage will render next frame
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW an ELLIPSE using EaselJs
  _me.easeljsdemoDrawingellipse = function(id) {
    //Create a stage by getting a reference to the canvas
    var stage = new createjs.Stage(id);
    //Create a Shape DisplayObject.
    var circle = new createjs.Shape();
    circle.graphics.setStrokeStyle(1).beginStroke(defaultStrokeColor).drawEllipse(renderdata.x * 2, renderdata.x * 2, renderdata.defaults.rw, renderdata.defaults.rh);
    //Set position of Shape instance.
    //Add Shape instance to stage display list.
    stage.addChild(circle);
    //Update stage will render next frame
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a RECTANGLE using EaselJs
  _me.easeljsdemoDrawingrectangle = function(id) {
    var stage = new createjs.Stage(id);
    var rect = new createjs.Shape();
    rect.graphics.setStrokeStyle(1).beginStroke(defaultStrokeColor).drawRect(renderdata.x, renderdata.y, renderdata.defaults.rw, renderdata.defaults.rh);
    stage.addChild(rect);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a SQUARE using EaselJs
  _me.easeljsdemoFillingsquares = function(id) {
    var stage = new createjs.Stage(id);
    var rect = new createjs.Shape();
    rect.graphics.beginFill(defaultFillColor).drawRect(renderdata.x, renderdata.y, renderdata.defaults.square, renderdata.defaults.square);
    stage.addChild(rect);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a RECTANGLE using EaselJs
  _me.easeljsdemoFillingrectangle = function(id) {
    var stage = new createjs.Stage(id);
    var rect = new createjs.Shape();
    rect.graphics.beginFill(defaultFillColor).drawRect(renderdata.x, renderdata.y, renderdata.defaults.rw, renderdata.defaults.rh);
    stage.addChild(rect);
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL a CIRCLE using EaselJs
  _me.easeljsdemoFillingcircle = function(id) {
    //Create a stage by getting a reference to the canvas
    var stage = new createjs.Stage(id);
    //Create a Shape DisplayObject.
    var circle = new createjs.Shape();
    circle.graphics.beginFill(defaultFillColor).drawCircle(renderdata.x * 2, renderdata.x * 2, (renderdata.defaults.square / 2));
    //Set position of Shape instance.
    circle.x = circle.y = 50;
    //Add Shape instance to stage display list.
    stage.addChild(circle);
    //Update stage will render next frame
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // FILL an ELLIPSE using EaselJs
  _me.easeljsdemoFillingellipse = function(id) {
    //Create a stage by getting a reference to the canvas
    var stage = new createjs.Stage(id);
    //Create a Shape DisplayObject.
    var circle = new createjs.Shape();
    circle.graphics.beginFill(defaultFillColor).drawEllipse(renderdata.x * 2, renderdata.x * 2, renderdata.defaults.rw, renderdata.defaults.rh);
    //Set position of Shape instance.
    //Add Shape instance to stage display list.
    stage.addChild(circle);
    //Update stage will render next frame
    stage.update();
    // Ignore this, it just hides the spinner!
    $('#' + id + '-loader').remove();
  };
  // DRAW a square using Konva
  _me.konvademoDrawingsquares = function(id) {
    var target = $('#' + id).parent().attr('id'); // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var sq = renderdata.defaults.square;
    // first we need to create a stage
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.canvasHeight,
      height: renderdata.defaults.canvasHeight
    });
    var layer = new Konva.Layer();
    // Now a RECT...
    var rect = new Konva.Rect({
      width: sq,
      height: sq,
      stroke: defaultStrokeColor,
      strokeWidth: 1,
      x: renderdata.x,
      y: renderdata.y
    });
    layer.add(rect);
    stage.add(layer);
  };
  // DRAW a POLYGON using Konva
  _me.konvademoDrawingpolygon = function(id) {
    var target = $('#' + id).parent().attr('id'); // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var stage = new Konva.Stage({
      container: target,
      width: renderdata.defaults.canvasHeight,
      height: renderdata.defaults.canvasHeight
    });
    var layer = new Konva.Layer();
    var hexagon = new Konva.RegularPolygon({
      x: renderdata.x * 5,
      y: renderdata.y * 5,
      sides: 3,
      radius: renderdata.defaults.rh - (renderdata.y),
      stroke: defaultStrokeColor,
      strokeWidth: 1
    });
    layer.add(hexagon);
    // add the layer to the stage
    stage.add(layer);
  };
  // ILL a POLYGON using Konva
  _me.konvademoFillingpolygon = function(id) {
    var target = $('#' + id).parent().attr('id'); // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var stage = new Konva.Stage({
      container: target,
      width: renderdata.defaults.canvasHeight,
      height: renderdata.defaults.canvasHeight
    });
    var layer = new Konva.Layer();
    var hexagon = new Konva.RegularPolygon({
      x: renderdata.x * 5,
      y: renderdata.y * 5,
      sides: 3,
      radius: renderdata.defaults.rh - (renderdata.y),
      fill: defaultFillColor
    });
    layer.add(hexagon);
    // add the layer to the stage
    stage.add(layer);
  };
  // DRAW a RECTANGLE using Konva
  _me.konvademoDrawingrectangle = function(id) {
    // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var target = $('#' + id).parent().attr('id');
    // first we need to create a stage
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.rw + (renderdata.x * 2),
      height: renderdata.defaults.canvasHeight
    });
    // Now we need a layer...
    var layer = new Konva.Layer();
    // Now a RECT...
    var rect = new Konva.Rect({
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      x: renderdata.x,
      y: renderdata.y,
      stroke: defaultStrokeColor,
      strokeWidth: 1
    });
    // Add the RECT to the LAYER..
    layer.add(rect);
    // Then add the LAYER to the STAGE
    stage.add(layer);
  };
  // DRAW a CIRCLE using Konva
  _me.konvademoDrawingcircle = function(id) {
    var target = $('#' + id).parent().attr('id');
    var width = renderdata.defaults.square;
    var height = renderdata.defaults.square;
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.square,
      height: renderdata.defaults.square
    });
    var layer = new Konva.Layer();
    var circle = new Konva.Circle({
      x: (renderdata.x + (renderdata.defaults.square / 2) - renderdata.x),
      y: (renderdata.y + (renderdata.defaults.square / 2) - renderdata.y),
      radius: ((renderdata.defaults.square / 2) - (renderdata.y / 2)),
      stroke: defaultStrokeColor,
      strokeWidth: 1,
    });
    // add the shape to the layer
    layer.add(circle);
    // add the layer to the stage
    stage.add(layer);
  };
  // DRAW an ELLIPSE using Konva
  _me.konvademoDrawingellipse = function(id) {
    var target = $('#' + id).parent().attr('id');
    var width = window.innerWidth;
    var height = window.innerHeight;
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.square * 2,
      height: renderdata.defaults.square
    });
    var layer = new Konva.Layer();
    var oval = new Konva.Ellipse({
      x: stage.getWidth() / 2,
      y: stage.getHeight() / 2,
      radius: {
        x: renderdata.defaults.square / 1.5,
        y: renderdata.defaults.square / 3
      },
      stroke: defaultStrokeColor,
      strokeWidth: 1
    });
    // add the shape to the layer
    layer.add(oval);
    // add the layer to the stage
    stage.add(layer);
  };
  // FILL an ELLIPSE using Konva
  _me.konvademoFillingellipse = function(id) {
    var target = $('#' + id).parent().attr('id');
    var width = window.innerWidth;
    var height = window.innerHeight;
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.square * 2,
      height: renderdata.defaults.square
    });
    var layer = new Konva.Layer();
    var oval = new Konva.Ellipse({
      x: stage.getWidth() / 2,
      y: stage.getHeight() / 2,
      radius: {
        x: renderdata.defaults.square / 1.5,
        y: renderdata.defaults.square / 3
      },
      fill: defaultFillColor
    });
    // add the shape to the layer
    layer.add(oval);
    // add the layer to the stage
    stage.add(layer);
  };
  // FILL a square using Konva
  _me.konvademoFillingsquares = function(id) {
    // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var target = $('#' + id).parent().attr('id');
    var sq = renderdata.defaults.square;
    // first we need to create a stage
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.canvasHeight,
      height: renderdata.defaults.canvasHeight
    });
    // Now we need a layer...
    var layer = new Konva.Layer();
    // Now a RECT...
    var rect = new Konva.Rect({
      width: sq,
      height: sq,
      fill: defaultFillColor,
      x: renderdata.x,
      y: renderdata.y,
    });
    // Add the RECT to the LAYER..
    layer.add(rect);
    // Then add the LAYER to the STAGE
    stage.add(layer);
  };
  // FILL a RECTANGLE using Konva
  _me.konvademoFillingrectangle = function(id) {
    // Konva targets DIVS, NOT Canvas elements, so we need to look up...
    var target = $('#' + id).parent().attr('id');
    // first we need to create a stage
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.rw + (renderdata.x * 2),
      height: renderdata.defaults.canvasHeight
    });
    // Now we need a layer...
    var layer = new Konva.Layer();
    // Now a RECT...
    var rect = new Konva.Rect({
      width: renderdata.defaults.rw,
      height: renderdata.defaults.rh,
      x: renderdata.x,
      y: renderdata.y,
      fill: defaultFillColor
    });
    // Add the RECT to the LAYER..
    layer.add(rect);
    // Then add the LAYER to the STAGE
    stage.add(layer);
  };
  // DRAW a CIRCLE using Konva
  _me.konvademoFillingcircle = function(id) {
    var target = $('#' + id).parent().attr('id');
    var width = renderdata.defaults.square;
    var height = renderdata.defaults.square;
    var stage = new Konva.Stage({
      container: target, // id of container
      width: renderdata.defaults.square,
      height: renderdata.defaults.square
    });
    var layer = new Konva.Layer();
    var circle = new Konva.Circle({
      x: (renderdata.x + (renderdata.defaults.square / 2) - renderdata.x),
      y: (renderdata.y + (renderdata.defaults.square / 2) - renderdata.y),
      radius: ((renderdata.defaults.square / 2) - (renderdata.y / 2)),
      fill: defaultFillColor
    });
    // add the shape to the layer
    layer.add(circle);
    // add the layer to the stage
    stage.add(layer);
  };
  // This method just sets up all of the CANVAS elements on the page...simples for now, but this WILL become more complex...
  _me.setupCanvasObjects = function(data) {
    // For every CANVAS ELEMENT with a CSS CLASS of "canvas-demo-item"...
    $('canvas.canvas-demo-item').each(function() {
      // Lets create a shortcut to THIS ELEMENT (its a CANVAS element)...saves on typing...remember CONTEXT of this and of  $(this)...
      var el = $(this);
      // el.width(el.parent().width());
      // Lets calculate our desired height based on the WINDOW height - At present, we are working to 15%...
      var newheight = renderdata.defaults.canvasHeight;
      // set the newly calculated height to the current CANVAS element...
      el.attr('height', newheight).height(newheight);
      // Ok, now we need to grab the name of the CALLBACK function that we need to fire for this CANVAS element
      var strCallbackName = $(this).attr('id').split('-')[0] + $(this).attr('id').split('-')[1].capitalizeFirstLetter();
      console.log(strCallbackName);
      // Right, we ONLY need to do the following IF we have a defined FUNCTION that has the name of the strCallbackName we have just declared...
      if (typeof _me[strCallbackName] === 'function') {
        // Add a spinner to roll while everything initialises...
        el.before('<div class="loader" id="' + el.attr('id') + '-loader"><i class="fa fa-spin fa-spinner"></div></div>');
        // Grab the CALLBACK function as a STRING so we can WRITE it out the document, so users can see how each sample works....
        $('#' + el.attr('id') + '-container').after('<div class="col-xs-12"><pre>' + _me[strCallbackName] + '</pre></div>');
        // Now, lets actually FIRE our callback...
        _me[strCallbackName](el.attr('id'));
      } else {
        // If we DONT have a FUNCTION with the strCallbackName, then let's just be polite and write out "Coming soon" in the required panel...
        el.before('<div class="loader">coming soon</div>');
      }
    });
    var toc = _.template(
      $("script.maintoc").html()
    );
    $('#main-toc').html(toc(data));
  };
  // Lets make life easier, and add some flexability by setting some of our values based on the users browser/UA
  _me.setDefaults = function() {
    var perc = 15;
    var canvasHeight = ($(window).height() / 100) * perc;
    var intPosX = renderdata.x;
    var intPosY = 20;
    var rectHeight = (canvasHeight - (intPosY * 4));
    var rectWidth = rectHeight * 2;
    var sq = canvasHeight - (intPosY * 2);
    var defaults = {
      numPerc: perc,
      canvasHeight: canvasHeight,
      square: sq,
      rw: rectWidth,
      rh: rectHeight
    };
    renderdata.defaults = defaults;
    // First time out, and as we are JSing wherever possible here to stay "in the js zone" while coding...lets make life easy by using JSON to build up the UI
    _me.loadData('mockdata/libraries.json', _me.renderIntroScreen);
  };
  // Fires after INIT, builds the intial screen...for our purposes, good enough for now...renders out _.template based on DATA..saves building
  // loads of HTML etc..
  // #nav-mark: renderIntroScreen
  _me.renderIntroScreen = function(data) {
    var template = _.template(
      $("script.sections").html()
    );

    $('#main-content').html(template(data)).promise().done(function() {
      // Once the template is sorted, we can do everything else...
      _me.setupCanvasObjects(data);
    });
  };
  // this is a DRY method, for GET requests on the JSON, expects a URL (relative ONLY due to XHR restrictions), and a CALLBACK which fires when the AJAX call successfully completes.
  // #nav-mark: loadData
  // #todo: ERROR HANDLING - what do we do when this load FAILS?
  _me.loadData = function(url, callback) {
    // CREATE our AJAX object....a GET on a JSON datasource...
    $.ajax({
      url: url,
      dataType: 'json',
      type: 'get',
      success: function(data) {
        callback(data);
      }
    });
  };
  _me.toggleDemoPanels = function() {
    $('div.panel-heading.toggle').each(function() {
      if (expanded === true) {
        $(this).parent().removeClass('closed');
      } else {
        $(this).parent().addClass('closed');
      }
    });
  };
  _me.scrollToAnchor = function(aid) {
    var offsetpoint = 80;
    if (aid === 'pagestart') {
      $('html,body').animate({
        scrollTop: 0
      }, 'fast');
    } else {
      $('html,body').animate({
        scrollTop: ($(aid).offset().top - offsetpoint)
      }, 'normal');
    }
  };
  // Self calling INIT...not 100% if this is the best way to go yet, but gives us something to work with!
  // #nav-mark: INIT
  // #todo: BEST PRACTICE - Is this the best INIT implementation?
  _me.init = (function() {
    _me.setDefaults();
    // Let's bind any events we need
    // Panel headings, these toggle content when CLICKED...
    body.delegate('div.panel-heading.toggle', 'click', function(event) {
      $(this).parent().toggleClass('closed');
    });
    // When the user clicks the "Expand all" button, we need to toggle the CSS classes, the expanded var and fire the handler function....
    body.delegate('#btn-toggle-all', 'click', function(event) {
      $(this).toggleClass('off on');
      if (expanded === true) {
        expanded = false;
      } else {
        expanded = true;
      }
      _me.toggleDemoPanels();
    });
    // Let's setup the TOC....
    body.delegate('a.toc-link', 'click', function(event) {
      _me.scrollToAnchor($(this).attr('href'));
      event.preventDefault();
      return false;
    });
  })();
  // EXPORT _me...
  return _me;
})(jQuery);
