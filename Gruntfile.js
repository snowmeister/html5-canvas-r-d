/*global module:false */
/*jshint -W030 */
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);


  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    /* Concatenate all js files into ONE file...MAKE SURE YOU ADD components here for inclusion with RELEASE script */
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          'bower_components/underscore/underscore.js',
          'bower_components/jquery/dist/jquery.js',
          'bower_components/bootstrap/dist/js/bootstrap.js',
          'bower_components/jcanvas/jcanvas.js',
          'bower_components/fabric.js/dist/fabric.js',
          'bower_components/paper/dist/paper-full.js',
          'bower_components/EaselJS/lib/easeljs-0.8.2.combined.js',
          'bower_components/konva/konva.js',
          'src/js/sce.js',
        ],
        dest: 'dist/js/<%= pkg.name %>.js'
      }
    },
    /* OK, when the file is concatenated, lets do a MINIFIED VERSION for production use.. */
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/js/<%= pkg.name %>.min.js': [
            '<%= concat.dist.dest %>',
          ]
        }
      }
    },
    /* Now lets setup the css - At this point, public CSS, but this COULD implment LESS or SASS at some point... */
    cssmin: {
      combine: {
        files: {
          'dist/css/<%= pkg.name %>.min.css': [
            'bower_components/bootstrap/dist/css/bootstrap.css',
            'bower_components/font-awesome/css/font-awesome.css',
            'src/css/*.css'
          ]
        }
      }
    },

    /* This is an important one. JSHINT all scripts to check for syntax errors, potential issues etc ... */
    jshint: {
      files: ['Gruntfile.js', 'src/js/*.js'],
      options: {

        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        browser: true,
        globals: {
          Chart: true,
          moment: true,
          jquery: true,
          BootstrapDialog: true,
          alert: true,
          jQuery: true,
          "console": true,
          require: true,
          define: true,
          requirejs: true,
          describe: true,
          expect: true,
          it: true,
          $: true,
          _: true,
          fabric: true,
          paper: true,
          createjs: true,
          Konva: true
        }
      }
    },
    /* We need to grab the FONTS from the bower_components directory - ADD here if you want to include new fonts to release... */
    copy: {
      files: {
        expand: true,
        flatten: true,
        src: ['bower_components/bootstrap/fonts/*',
          'bower_components/font-awesome/fonts/*'
        ], // set working folder / root to copy

        dest: 'dist/fonts', // destination folder

      },
      html: {
        expand: true,
        flatten: true,
        src: 'src/index.html',
        dest: 'dist/'
      },
      mockdata: {
        expand: true,
        flatten: true,
        src: 'src/mockdata/*.json',
        dest: 'dist/mockdata/'
      },
    },
    /* Create the watch to use during development, in this instance we will be using the 'preview' task, on the main css and all js in the src/ directory */
    watch: {
      scripts: {
        files: ['<%= jshint.files %>', 'src/css/*.css', 'src/css/exterity/*.css', 'src/js/*.js', 'src/tests/**/*.html', 'src/tests/js/*.js', 'src/*.html', 'src/mockdata/*.json'],
        tasks: ['preview'],
        options: {
          spawn: false,
        },
      }
    }
  });
  // Load JSHint task
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-htmlmin');
  // Default task.
  //  grunt.registerTask('default', ['uglify', 'cssmin', 'htmlmin']);
  grunt.registerTask('preview', ['jshint', 'concat', 'uglify', 'copy',
    'cssmin'
  ]);
  grunt.registerTask('quickbuild', ['jshint', 'concat', 'uglify', 'copy']);
  //grunt.registerTask('preview', ['jshint', 'concat', 'cssmin']);
  grunt.registerTask('default', 'watch');
  grunt.registerTask('devbuild', ['makelib']);

};
