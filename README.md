Snowmeister's Canvas Experiments
===================
[TOC]

Hey! Thanks for checking this repo out. This repo is purely for R&D code, and nothing you find here is intended to be used for ANY production code.

If, like me, you are interesting in exploring the possibilities of the HTML5 Canvas element, and getting up to speed with the concepts, I hope this repo may prove useful to you!

You can see the code running in my [online Lab](http://lab.snowmeister.co.uk/canvas/index.html)

----------

Introduction
-------------
As a full-time full-stack developer, I need to "keep my claws sharp" as much as possible and stay up-to-date with all the latest and emerging techniques and technologies. Much of the time, this means learning the minimum required to "get the job done", but this unfortunately often makes it difficult to gain a deeper understanding of some techniques and technologies.

In my full time role I have been working extensively with the HTML5 canvas element recently, and as I started as a bit of a Flash specialist (back in the mid-late 90s), and have seen the fall of interactive animations, visual effects etc as Flash has dropped from the popular platform it once was, to now being a bit of a dirty word in web design and development, I am keen to see IF the HTML5 Canvas element can match up to it, for dynamic images, games, rich interaction etc.

As a side project, I have been working with my 14yr old son on building a web-based game, so the HTML5 Canvas is a tasty proposition if it can help with some of the rich interaction and data-driven graphics etc that will be required for the game.

With the above in mind, the object of this repo is to explore HTML5 Canvas, learn some new tricks and if all goes to plan, I intend to build an entirely Canvas based personal demo site with the knowledge I hope to gain while building the samples and experiments I envision being part of this repo.

Another thing I hope to that this repo will illustrate to beginners and less experienced coders, is that "there is more than one way to skin a cat" (apologies to cat lovers, no felines have been hurt in the making of this repo...). Something that any coder with a bit of intelligence and experience will tell you - There are often MANY ways of achieving a given task, but the trick is finding the quickest, easiest, and most efficient solution! (Dont forget stability and testability too!)

> **Please Note:**

> - This repo is a PERSONAL PROJECT, and you are more than welcome to FORK, but I will not be accepting any pull requests for this repo, but who knows I may need some help/collaboration as I progress...

> - As a PERSONAL PROJECT, I will working on this as and when I get chance, which will only be a few hours a month, so please, don't expect rapidly progressing development, or ask for patches, updates, support etc. IF all goes to plan, I will eventually be posting some supporting wiki articles, but there's no point me doing that until I can provide solid, accurate documentation and samples!
### Things I intend to be exploring

 1. The BASICS. Using CANVAS without libraries, sussing out the nuances and things to consider...
 1. Cross-browser and device support
 1. Performance gotchas
 1. Masking, opacity and other visual effect possibilities
 1. Animation, based on User interaction, and programmatic control
 1. Loading images and videos
 1. Creating sprites
 1. Co-ordinates. Each library seems to use differing co-ordinating systems for positioning and drawing etc. This is important, if its hard to work out and use position, sizing etc, then the project velocity decreases! These demos and experiments will highlight some of these differences.
 1. Event Handling (dragging and dropping, collision detection, mouse and touch events etc)
 1. Quick'n'dirty ways of creating CANVAS graphics (Adobe Animate CC may play a part here...)
 1. SVG to CANVAS mapping/conversion
 1.  The various libraries available..including
    1. [jCanvas](http://projects.calebevans.me/jcanvas/) - I have been using jCanvas as part of my work at Enabledware, building a UI for designing digital signage, but have only really scratched the surface so far, so I am keen to see how far I can push it, and too learn the best way of achieving our product functionality requirements
    1. [FabricJS](http://fabricjs.com/) - I have played with this library a little, and it looks pretty slick, so I'm keen to explore further...particularly for the SVG to Canvas (and Canvas to SVG) conversion capabilities
    1. [PaperJS](http://paperjs.org/) - I've only recently seen this library, but it looks very interesting, not only for it's SVG to Canvas and Canvas to SVG, but also for it's SYMBOLS etc. The documentation looks pretty good too, which is always a Plus Point
    1. [EaselJs](http://www.createjs.com/easeljs) - Never tried this one, but I have read a few articles, only due to an interest in how Flash developers migrate to more modern standards etc...It looks interesting, but not sure at all about this yet, as it is the library used by Adobe for their Canvas export from Animate.cc, which suggests lots of Flash related bloat etc. Should be interesting to see how this stacks up against the other candidates,
    1. [Konva](https://github.com/konvajs/konva) - I've only just discovered this one as part of this R&D work, but it looks pretty good, and is under active development, which makes it definately worth further investigation
    1. and more....Part of this will involve repeating the same task or
    technique using each library, to be able to directly compare each, and make recommendations, and gain an understanding of the pros and cons of each library or technique.

----------

## Tools & Frameworks Used

I'm a big fan of automation and reducing effort wherever possible.  As a developer, I have enough to think about with solving problems, providing solutions and writing good code, without having to worry about the day-to-day stuff like dependencies management, code organisation, best practice etc, so I tend to find tools that make life easier wherever I can.

Gone are the days of FTP, manually concatentating files, checking syntax etc, today as web developers,  we have a huge array of fantastic tools available, that while might look like more trouble than they could be worth at first glance, but actually help us produce better, faster, higher-quality code, with less effort.

So, if you DO decide to fork this repo, and you've not used tools like [GRUNT](http://gruntjs.com/) or [BOWER](https://bower.io/), then you might have a bit of a learning curve while you grasp how the tooling works, but stick with it, I promise you, once you see how these tools can improve your life, you WILL start finding ways of including them in your workflow.

I am also a firm believer in "building on the shoulders of giants" - Yes, you may well *be able* to write a perfectly good library or framework from scratch (if you need one), but that doesn't necessarily mean that you *should*! Libraries and frameworks such as Bootstrap, jQuery, React, Angular etc are all built and tested by some of the most ninja developers, at some of successful companies on the web, and as such can be considered solid, and reliable, or in some cases, weapons-grade. Why re-invent the wheel when someone has already gone through the pain of solving the same problem you are working on?

### Prerequisites

You will need the following installed on your development machine to get the best from this repo.

1. [NodeJS](https://nodejs.org/en/) - Server-side (and desktop) JS engine, based on the Chrome Browser's V8 JavaScript engine.
2. [NPM](https://www.npmjs.com/package/npm) - This is the defacto NodeJS package manager. If you have a later build of NodeJS, this will already be available to you on your system
2. [BOWER](https://bower.io/) - Bower is a package (dependencies) manager for web based projects
3.  [GRUNT](http://gruntjs.com/) - One of the tools I simply cannot live without, GRUNT is a JavaScript task runner, used to run common tasks such as Linting, obfuscation, moving files to production directories etc...
4. [GIT](https://git-scm.com/) - Obviously!

## Getting Started
Assuming you have the above prerequisites installed and configured correctly, you can get started pretty quickly by following these steps...

1. Clone this repo
2. NPM install to grab any NodeJS dependencies
3. BOWER install to grab any other dependencies
4. TODO - How to RUN the samples etc - for now, you should be able to sort this yourself....
